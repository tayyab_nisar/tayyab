const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const PORT = 3000;


require('./src/connection/connection');
const { addNewRest, addNewOrder, getRestOrders, deleteRestOrder, updateOrder } = require('./src/main');
require('./src/Q02-01');
require('./Q03');
require('./Q02')


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
app.use((error, req, res, next) => {
    res.status(error.status | 500).json({ error: error.message });
    next();
})

app.post('/addNewRestaurent', addNewRest );
app.post('/addNewOrder', addNewOrder);
app.get('/getOrders/:restName', getRestOrders);
app.delete('/deleteOrderOfResturent/:restName/:orderId', deleteRestOrder);
app.post('/updateOrder/:restName/:orderId', updateOrder)
app.get('/', (req, res) => {
    res.status(200).send('i am listening on web')
})
app.listen(PORT, () => {
    console.log('server is started');
})