const Sequelize = require("sequelize");

module.exports = sequelize.define("Orders", {
    id: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },
    order: Sequelize.STRING(300),
    status : Sequelize.BOOLEAN
});