const Sequelize = require("sequelize");

module.exports = sequelize.define("Rest", {
    id: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },
    name: {
        type: Sequelize.STRING(35),
        allowNull: false,
        unique: true
    }
});