const Orders = require("./Models/orders");
const Rest = require("./Models/restaurents");

Rest.hasMany(Orders, { as: "Orders", foreignKey: "restId" });
Orders.belongsTo(Rest, { as: "Rest", foreignKey: "restId" });
const addNewRest = async (req,res,next)=>{
    try {
        const add = await Rest.create({
            name: req.body.name
        });
        res.status(200).json(add)
    } catch (error) {
        next(error)
    }
};
const addNewOrder = async (req, res, next) => {
    try {
        const rest = await Rest.findOne({ where: { name: req.body.name } });
       if(!rest){
           let error= new Error('can not find Restaurent');
           error.status=400;
           throw error;
       }
       else{
           let newOrder = await Orders.create({ order: req.body.order,status:true, restId:rest.id});
           return res.status(200).json(newOrder)
       }
        
    } catch (error) {
        next(error);
    }
};
const getRestOrders  = async (req,res,next)=>{
    try {
        let rest = req.params.restName;
        const rests = await Rest.findOne({ where: { name: rest } });
        if (!rests) {
            let error = new Error('can not find Restaurent');
            error.status = 400;
            throw error;
        } 
        const findOrders = await Orders.findAll({where:{restId:rests.id}});
        return res.status(200).json({orders:findOrders});
        
    } catch (error) {
        next(eror)
    }
}
const deleteRestOrder = async (req, res, next) => {
    try {
        let rest = req.params.restName;
        const rests = await Rest.findOne({ where: { name: rest } });
        if (!rests) {
            let error = new Error('can not find Restaurent');
            error.status = 400;
            throw error;
        }
        const deleteOrders = await Orders.destroy({ where: { restId: rests.id, id: req.params.orderId } });
        return res.status(200).json({ orders: deleteOrders });

    } catch (error) {
        next(eror)
    }
}
const updateOrder = async (req,res,next)=>{
    try {
        let rest = req.params.restName;
        let orderId=req.params.orderId;
        const rests = await Rest.findOne({ where: { name: rest } });
        if (!rests) {
            let error = new Error('can not find Restaurent');
            error.status = 400;
            throw error;
        }
        let update = await Orders.update({order:req.body.order,status:req.body.status},{where:{
            restId:rests.id,
            id:orderId
        }});
        return res.status(200).json(update);

        
    } catch (error) {
        next(error);
    }
}
module.exports = { addNewRest, addNewOrder, getRestOrders, deleteRestOrder, updateOrder}